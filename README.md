# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


spec test:
docker-compose run --rm website bundle exec rspec spec/services/raffle_service_spec.rb

docker-compose run --rm website bundle install



prod:

docker-compose -f docker-compose-prod.yml run --rm app bundle exec rake db:create db:migrate --build