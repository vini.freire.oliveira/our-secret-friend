require 'rails_helper'

RSpec.describe MembersController, type: :controller do

  describe "POST #create" do
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "DELETE #destroy" do
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "PUT #update" do
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

end
